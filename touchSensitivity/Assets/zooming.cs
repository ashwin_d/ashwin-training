﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class zooming : MonoBehaviour {

    public Canvas canvas; 
    public float zoomSpeed = 0.5f;       

    public float resetDuration = 3.0f;
    float durationTimer = 0.0f;

    float startScale = 0.0f;

    // Use this for initialization
    void Start () {
        startScale = canvas.scaleFactor;

    }

 
    void Update()
    {
        
        if (Input.touchCount == 2)
        {
           
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);

           
            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

            
            float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

         
            canvas.scaleFactor -= deltaMagnitudeDiff * zoomSpeed;

            canvas.scaleFactor = Mathf.Max(canvas.scaleFactor, startScale);
            canvas.scaleFactor = Mathf.Min(canvas.scaleFactor, startScale * 3.0f);

            durationTimer = 0.0f;
        }
        else
        {
            durationTimer += Time.deltaTime;

            if (durationTimer >= resetDuration)
            {
                canvas.scaleFactor = startScale;
            }
        }

    }

}

