﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class touch : MonoBehaviour
{

    public float maxtime;
    public float minSwipeDistance;
  
    float startTime, endTime;
    int x, y, z;

    Vector3 startPos, endPos; //3 axis


    float swipeDistance;
    float swipeTime;

    public GameObject Eny1,Eny2,Eny3;

    private int Score;
    public Text point;


    public bool DownSwipe = false;
    public bool UpSwipe = false;
    public bool RightSwipe = false;
    public bool LeftSwipe = false;

    private bool Down = true;
    private bool Up = true;
    private bool Right =true;
    private bool Left = true;

   // public float perspectiveZoomSpeed = 0.5f;        // The rate of change of the field of view in perspective mode.
//public float orthoZoomSpeed = 0.5f;        // The rate of change of the orthographic size in orthographic mode.

    public bool Pause = true;
    public GameObject PanelPAuse;
    public bool PanelPause = true;

    public bool but = true;
    public GameObject pas1;

    // Use this for initialization
    void Start()
    {
       

        /* if(TouchPhase.Began == 0)
         {
             for (int i = 0; i < 5; i++)
             {
                 Debug.Log("Touch began");
             }
         }
         else
         {
             Debug.Log("Touch not began");
         }*/
    }

    // Update is called once per frame
    void Update()

    {
        if (Time.timeScale > 0)
        {
            transform.position += new Vector3(x, y, z);
        }

        if (Input.touchCount > 0)
        {

            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                startTime = Time.time;
                startPos = touch.position;

            }
            else if (touch.phase == TouchPhase.Ended)
            {
                endTime = Time.time;
                endPos = touch.position;

                swipeDistance = (endPos - startPos).magnitude;
                swipeTime = endTime - startTime;

                if (swipeTime < maxtime && swipeDistance > minSwipeDistance)
                {
                    swipe();    //call swipe function
                }


            }
        }

    }

    void swipe()
    {

       
        Debug.Log("swipe");
        Vector2 distance = endPos - startPos;
        if (Mathf.Abs(distance.x) > Mathf.Abs(distance.y))
        {
            Debug.Log("horizontal swipe");
            if (distance.x > 0)

            {
                TaskOnClickRight();
            }

            else if (distance.x < 0)

            {
                TaskOnClickLeft();

            }
        }
        else if (Mathf.Abs(distance.x) < Mathf.Abs(distance.y))
        {
            Debug.Log("Vertical swipe");

            if (distance.y > 0)

            {

                TaskOnClickUp();


            }
            else if (distance.y < 0)
            {

                TaskOnClickDown();
            }
        }
    }

    public void pannel() //pause button is clicked enter the new pannel
    {
        Debug.Log("pannel entering");
        if (Pause )
        {
            PanelPAuse.SetActive(true);
           

            Pause = false;
            Debug.Log("show the pannel");
        }
        else
        {
            PanelPAuse.SetActive(false);
            Pause = true;
            Debug.Log("unshow the pannel");
        }

    }
  /*   public void pannel2()
      { 
          if (but)
          {
                  
              Debug.Log(" pannel 2");
          }

      }*/

    public void pause()
    {
       
        if (Pause)
        {
            Debug.Log("the game is  pause");
            Time.timeScale = 0;    //game object is pause
      

        }

        else
        {
            Debug.Log("the game is play ");
            Time.timeScale = 1;   //game object is play
     

        }

       pannel();  //call the pannel function

    }


    public void TaskOnClickUp() //upbutton clicked gameobject moved up 
    {
        if (Up)
        {
            Debug.Log("Up click");
            Down = true;
            x = 0;
            y = +10;
            z = 0;
        }
    }
    public void TaskOnClickDown()    //upbutton clicked gameobject moved down
    {
        if (Down)
        {
            Up = true;
            Debug.Log("Down click");
            x = 0;
            y = -10;
            z = 0;
        }
        

    }
    public void TaskOnClickLeft()   //upbutton clicked gameobject moved left
    {
        if (Left)
        {
            Right = true;
            Debug.Log("Up click");
            x = -10;
            y = 0;
            z = 0;
        }
       


    }
    public void TaskOnClickRight()  //upbutton clicked gameobject moved right
    {
      
        if (Right)
        {
            Left = true;
            Debug.Log("Right click");
            x = +10;
            y = 0;
            z = 0;
        }
       


    }

    void OnTriggerEnter2D(Collider2D col) //if collision is occurs this function will true
    {


        if (col.gameObject.name == "down")
        {
            Down = false;
            x = 0;
            y = 0;
            z = 0;

            DownSwipe = false;

        }
        if (col.gameObject.name == "up")
        {

            Up = false;
            x = 0;
            y = 0;
            z = 0;

            UpSwipe = false;

        }
        if (col.gameObject.name == "right")
        {
            Right = false;
            x = 0;
            y = 0;
            z = 0;
            RightSwipe = false;

        }
        if (col.gameObject.name == "left")
        {
            Left = false;
            x = 0;
            y = 0;
            z = 0;
            LeftSwipe = false;

        }

        if (col.gameObject.name == "eny1")
        {
            
            Destroy(Eny1);
            Score = 2;
            Debug.Log(+Score);
          

        }
            
        else
        if (col.gameObject.name == "eny2")
        {
            
            Destroy(Eny2);
            Score = 5;
            Debug.Log(+Score);
           
        }
        else
        if (col.gameObject.name == "eny3")
        {
            
            Destroy(Eny3);
           
            Score = 7;
            Debug.Log(+Score);
            
        }
        Score++;
        point.text = "Score=" + Score.ToString();
        UpdateScore();

    }
  

    public void UpdateScore()
    {
        point.text = "Score: " + Score;
    }
 

    public void backtoGameScene()     //the another scene back button is press this function is call
    {
        Application.LoadLevel(1);      //go to next scene
    } 


 }

    /*  void Update () {
       Touch myTouch = Input.GetTouch(0);

       Touch[] myTouches = Input.touches;

       if (int i = 0; i < Input.touchCount; i++)
       {
           transform.position+= new Vector3(-1, 0,0);

       }
       if (Input.GetKeyDown(KeyCode.UpArrow))
       {
           transform.position += new Vector3(0, 1, 0);

       }
       if (Input.GetKeyDown(KeyCode.RightArrow))
       {
           transform.position += new Vector3(1, 0, 0);



       }

       if (Input.GetKeyDown(KeyCode.DownArrow))
       {
           transform.position += new Vector3(0, -1, 0);
       }

   }
   void OnTriggerEnter2D (Collider2D col)
   {
       if (col.gameObject.name == "rock")
       {
           Debug.Log("rock");
           //transform.position += new Vector3(-1, 0, 0);
           Destroy(col.gameObject);
           point = 2;
              //UnityScript
           Debug.Log(+point);
       }
       else if (col.gameObject.name == "jack")
       {
           Debug.Log("jack");
           Destroy(col.gameObject);
           point = 3;//point=point+2

           Debug.Log(+point);

       }
       else if (col.gameObject.name == "mack")
       {
           Debug.Log("mack");
           Destroy(col.gameObject);
           point = 4;


       }
       scoreText.text = "Score=" + point.ToString();
       Debug.Log("Score=" + scoreText.text);


   }*/





